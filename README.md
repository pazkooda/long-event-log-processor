Long running events log processor
===

This program searches for long running events (> 3 ms) build in structure like below:

```json
{"id":"scsmbstgra", "state":"STARTED", "type":"APPLICATION_LOG", "host":"12345", "timestamp":1491377495212}
{"id":"scsmbstgrb", "state":"STARTED", "timestamp":1491377495213}
{"id":"scsmbstgrc", "state":"FINISHED", "timestamp":1491377495218}
{"id":"scsmbstgra", "state":"FINISHED", "type":"APPLICATION_LOG", "host":"12345", "timestamp":1491377495217}
{"id":"scsmbstgrc", "state":"STARTED", "timestamp":1491377495210}
{"id":"scsmbstgrb", "state":"FINISHED", "timestamp":1491377495216}
```

# How to run
## From Maven
For example in PowerShell one can invoke:
```powershell
.\mvnw spring-boot:run -D"spring-boot.run.arguments=C:\sample\log\path\events.log"
```

## Run app directly
To run app directly you need to build it first with:

```powershell
.\mvnw clean package
``` 

Once package is build one can run Java app directly from JAR build in `target` directory:

```powershell
java -jar long-event-analyzer-0.0.1-SNAPSHOT.jar "C:\sample\log\path\events.log"
```


