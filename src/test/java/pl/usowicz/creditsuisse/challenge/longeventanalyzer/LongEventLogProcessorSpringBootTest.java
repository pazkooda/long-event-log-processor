package pl.usowicz.creditsuisse.challenge.longeventanalyzer;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.usowicz.creditsuisse.challenge.longeventanalyzer.Alert.Builder.anAlert;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations="classpath:test.properties")
public class LongEventLogProcessorSpringBootTest {

    @Autowired
    private LongEventLogProcessor logProcessor;

    @Autowired
    private AlertRepository alertRepository;

    @Before
    public void beforeEach() {
        alertRepository.deleteAll();
    }

    @Test
    public void process() throws URISyntaxException, IOException {
        Path filePath = Paths.get(LongEventLogProcessorSpringBootTest.class.getClassLoader().getResource("data.log").toURI());

        logProcessor.process(filePath);

        List<Alert> alerts = new LinkedList<>();
        alertRepository.findAll().forEach(alerts::add);

        Assertions.assertThat(alerts)
                .usingElementComparatorIgnoringFields("id")
                .containsExactlyInAnyOrder(
                        anAlert()
                                .eventId("scsmbstgra")
                                .type("APPLICATION_LOG")
                                .duration(5L)
                                .host("12345")
                                .build(),
                        anAlert()
                                .eventId("scsmbstgrc")
                                .duration(8L)
                                .build());
    }

}
