package pl.usowicz.creditsuisse.challenge.longeventanalyzer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LongEventLogProcessorTest {

    @InjectMocks
    private LongEventLogProcessor logProcessor = new LongEventLogProcessor();

    @Mock
    private AlertRepository alertRepository;

    private ArgumentCaptor<Alert> alertArgCaptor = ArgumentCaptor.forClass(Alert.class);

    @Test
    public void process() throws URISyntaxException, IOException {
        Path filePath = Paths.get(LongEventLogProcessorTest.class.getClassLoader().getResource("data.log").toURI());

        logProcessor.process(filePath);

        verify(alertRepository, times(2)).save(alertArgCaptor.capture());

        List<Alert> capturedAlerts = alertArgCaptor.getAllValues();

        assertThat(capturedAlerts)
                .usingElementComparatorIgnoringFields("id")
                .containsExactlyInAnyOrder(
                        Alert.Builder.anAlert()
                                .eventId("scsmbstgra")
                                .type("APPLICATION_LOG")
                                .duration(5L)
                                .host("12345")
                                .build(),
                        Alert.Builder.anAlert()
                                .eventId("scsmbstgrc")
                                .duration(8L)
                                .build());
    }
}
