package pl.usowicz.creditsuisse.challenge.longeventanalyzer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Component
public class LongEventLogProcessor {

    private static final int DURATION_ALERT_THRESHOLD = 3;

    private static final Logger log = LoggerFactory.getLogger(LongEventLogProcessor.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private AlertRepository alertRepository;

    public void process(Path filePath) throws IOException {
        Map<String, LogEntry> knownEvents = new HashMap<>();
        Files.lines(filePath)
                .forEach(line -> {
                    try {
                        LogEntry currEntry = mapper.readValue(line, LogEntry.class);
                        String eventId = currEntry.getId();
                        if (knownEvents.containsKey(eventId)) {
                            Long duration = duration(currEntry, knownEvents.get(eventId));
                            if (duration > DURATION_ALERT_THRESHOLD) {
                                persistAlert(currEntry, duration);
                            }
                            knownEvents.remove(eventId);
                        } else {
                            knownEvents.put(eventId, currEntry);
                        }
                    } catch (JsonProcessingException e) {
                        log.warn("Unable to process line: {}", line);
                    }
                });
    }

    private void persistAlert(LogEntry logEntry, Long duration) {
        Alert alert = Alert.Builder.anAlert()
                .eventId(logEntry.getId())
                .duration(duration)
                .host(logEntry.getHost())
                .type(logEntry.getType())
                .build();
        alertRepository.save(alert);
    }

    private long duration(LogEntry entry1, LogEntry entry2) {
        return Math.abs(entry1.getTimestamp() - entry2.getTimestamp());
    }

}
