package pl.usowicz.creditsuisse.challenge.longeventanalyzer;

import javax.persistence.*;

@Entity
public class Alert {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String eventId;

    @Column
    private String type;

    @Column
    private Long duration;

    @Column
    private String host;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "id=" + id +
                ", eventId='" + eventId + '\'' +
                ", type='" + type + '\'' +
                ", duration=" + duration +
                ", host='" + host + '\'' +
                '}';
    }

    public static final class Builder {
        private String eventId;
        private String type;
        private Long duration;
        private String host;

        private Builder() {
        }

        public static Builder anAlert() {
            return new Builder();
        }

        public Builder eventId(String eventId) {
            this.eventId = eventId;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder duration(Long duration) {
            this.duration = duration;
            return this;
        }

        public Builder host(String host) {
            this.host = host;
            return this;
        }

        public Alert build() {
            Alert alert = new Alert();
            alert.setEventId(eventId);
            alert.setType(type);
            alert.setDuration(duration);
            alert.setHost(host);
            return alert;
        }
    }
}
