package pl.usowicz.creditsuisse.challenge.longeventanalyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LongEventAnalyzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LongEventAnalyzerApplication.class, args);
	}

}
