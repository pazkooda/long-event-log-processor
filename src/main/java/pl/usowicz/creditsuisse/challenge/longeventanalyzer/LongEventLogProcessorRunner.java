package pl.usowicz.creditsuisse.challenge.longeventanalyzer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Files.notExists;

@Component
public class LongEventLogProcessorRunner implements ApplicationRunner {

    private Logger log = LoggerFactory.getLogger(LongEventLogProcessorRunner.class);

    @Autowired
    private LongEventLogProcessor logProcessor;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (args.getNonOptionArgs().size() != 1) {
            log.error("This programs requires single command line argument being path to log file location. Nothing will be processed.");
            return;
        }

        String logLocation = args.getNonOptionArgs().get(0);
        Path filePath = Paths.get(logLocation);

        if (notExists(filePath)) {
            log.error("No file found under provided location: {}", logLocation);
            return;
        }

        logProcessor.process(filePath);

    }

}
