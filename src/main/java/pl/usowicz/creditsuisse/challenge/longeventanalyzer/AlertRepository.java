package pl.usowicz.creditsuisse.challenge.longeventanalyzer;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface AlertRepository extends CrudRepository<Alert, String> {}
